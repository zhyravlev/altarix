(function () {
  'use strict';

  angular
    .module('altarix')
    .service('weatherService', weatherService);

  /** @ngInject */
  function weatherService() {
    var weatherService = this;
    var data = {};

    weatherService.get = function(){
      return data;
    };

    weatherService.getForCity = function(key){
      if(key in data) {
        return data[key];
      }

      return false;
    };

    weatherService.new = function(key){
      return data[key] = [];
    };
  }
})();
