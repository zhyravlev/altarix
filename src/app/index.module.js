(function() {
  'use strict';

  angular
    .module('altarix', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ngResource', 'ui.router', 'ngMaterial', 'ngLodash']);

})();
