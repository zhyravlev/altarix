/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('altarix')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
