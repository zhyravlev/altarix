(function () {
  'use strict';

  angular
    .module('altarix')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($scope, $mdDialog, $log, moment, lodash, weatherService) {
    var ctrl = this;

    ctrl.currentCity = false;
    ctrl.weather = weatherService.get(); // For view only
    ctrl.cities = loadAll();
    ctrl.search = search;
    ctrl.inWeather = inWeather;
    ctrl.addWeather = addWeather;
    ctrl.removeWeather = removeWeather;
    ctrl.editWeather = editWeather;
    ctrl.parseDate = parseDate;

    function parseDate(date){
      return moment(date).format('DD/MM/YYYY');
    }

    function editWeather(item, ev) {
      $mdDialog.show({
        controller: 'weatherDialogController',
        controllerAs: 'ctrl',
        templateUrl: 'app/components/weather.dialog/weather.dialog.html',
        targetEvent: ev,
        clickOutsideToClose: true,
        locals: {
          city: ctrl.currentCity,
          weather: item,
          asEdit: true
        }
      });
    }

    function removeWeather(item) {
      var index = weatherService.getForCity(ctrl.currentCity.value).indexOf(item);
      if (index > -1) {
        weatherService.getForCity(ctrl.currentCity.value).splice(index, 1);
      }
    }

    function addWeather(ev) {
      $mdDialog.show({
          controller: 'weatherDialogController',
          controllerAs: 'ctrl',
          templateUrl: 'app/components/weather.dialog/weather.dialog.html',
          targetEvent: ev,
          clickOutsideToClose: true,
          locals: {
            city: ctrl.currentCity,
            weather: null,
            asEdit: false
          }
        })
        .then(function (result) {
          if (!angular.isArray(weatherService.getForCity(ctrl.currentCity.value))) {
            return weatherService.new(ctrl.currentCity.value).push(result);
          }

          var find = lodash.find(weatherService.getForCity(ctrl.currentCity.value), function (weather) {
            return moment(weather.date).diff(moment(result.date), 'days') === 0;
          });

          if (angular.isUndefined(find)) {
            var data;
            if ((data = weatherService.getForCity(ctrl.currentCity.value)) && angular.isArray(data)) {
              return data.push(result);
            }

            return $log.error('Key `' + ctrl.currentCity.value + '` doesn`t exists');
          }

          return $log.warn('Date ' + find.date + ' for city ' + ctrl.currentCity.display + ' is exists!');
        });
    }

    function inWeather(city) {
      return weatherService.getForCity(city);
    }

    function search(query) {
      return query ? lodash.filter(ctrl.cities, function (city) {
        return city.value.indexOf(angular.lowercase(query)) === 0
      }) : ctrl.cities;
    }

    function loadAll() {
      var cities = [
        'Пенза',
        'Саранск',
        'Helsinki',
        'Новосибирс',
        'Москва',
        'Санкт-Петербург',
        'Иркутск',
        'Stockholm',
        'Сочи',
        'Tallinn'
      ], arr = [];

      lodash.forEach(cities, function (city) {
        arr.push({
          value: angular.lowercase(city),
          display: city
        });
      });

      return arr;
    }

    var dieSelectedCity = $scope.$watch('ctrl.selectedItem', function (newValue) {
      if (angular.isDefined(newValue)) {
        ctrl.currentCity = newValue;
      }
    });

    $scope.$on('$destroy', dieSelectedCity);
  }
})();
