(function () {
  'use strict';

  angular
    .module('altarix')
    .controller('weatherDialogController', weatherDialogController);

  /** @ngInject */
  function weatherDialogController($mdDialog, lodash, moment, city, weather, asEdit, weatherService) {
    var ctrl = this;

    ctrl.weather = weather || {};
    ctrl.asEdit = asEdit;
    ctrl.cancel = cancel;
    ctrl.add = add;
    ctrl.checkDate = checkDate;

    function cancel() {
      return $mdDialog.cancel();
    }

    function add(weatherForm) {
      if(weatherForm.$valid){
        $mdDialog.hide(ctrl.weather);
      }
    }

    function checkDate(date) {
      var find = lodash.find(weatherService.getForCity(city.value), function (weather) {
        return moment(weather.date).diff(moment(date), 'days') === 0;
      });

      if (angular.isUndefined(find)) {
        return true;
      }

      if(asEdit){
        return moment(weather.date).diff(moment(date), 'days') === 0;
      }

      return false;
    }
  }
})();
