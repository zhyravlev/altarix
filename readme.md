**Installation**
---
By your computer should be installed: `node`, `npm`, `bower`, `gulp`
Install NodeJS and NPM from the official site: [nodejs.org](https://nodejs.org/en/)
Then using the console install: `$ npm install -g bower gulp`
Clone this repository: `$ git clone https://github.com/ZhyravlevAS/altarix.git`
Go to project: `$ cd altarix`
And run: `$ npm install && bower install`

**For develop**
---
Run develop serve: `$ gulp serve`

**For production**
---
Run: `$ gulp dist` or `$ gulp serve:dist` for build and run locally